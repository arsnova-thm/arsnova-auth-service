package de.thm.arsnova.service.authservice.model.command

class DeleteRoomAccessCommand (
        val roomId: String,
        val userId: String
)
